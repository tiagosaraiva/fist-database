import React from 'react';
import styled from 'styled-components';
import { FaArrowLeft } from 'react-icons/fa';
import { Link } from 'react-router-dom';

const HeaderContainer = styled.div`
  width: 100%;
  padding: 10px;
  background-color: transparent;
  display: flex;
  align-items: center;
`;

const BackButton = styled(Link)`
  font-size: 24px;
  color: #000;
  text-decoration: none;
  display: flex;
  align-items: center;

  &:hover {
    color: #007bff;
  }
`;

const Icon = styled(FaArrowLeft)`
  margin-right: 10px;
`;

const HeaderComponent = () => (
  <HeaderContainer>
    <BackButton to="/">
      <Icon />
    </BackButton>
  </HeaderContainer>
);

export default HeaderComponent;
