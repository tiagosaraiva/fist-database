import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { HashRouter,BrowserRouter } from 'react-router-dom';

const Router =
  process.env.NODE_ENV === 'development' ? BrowserRouter : HashRouter;

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Router>
  <App />
  </Router>
);
