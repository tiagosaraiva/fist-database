import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import VideosPage from '../pages/VideosPage';
import AudiosPage from '../pages/AudiosPage';
import HomePage from '../pages/HomePage';

export const ROUTES = {
    HOME: '/',
    VIDEOS: '/videos_page',
    AUDIOS: '/audios_page',
};

const RoutesFile = () => {

    return (
        <Routes>
            <Route
                path={ROUTES.HOME}
                element={<HomePage />}
                exact
            />

            <Route
                path={ROUTES.VIDEOS}
                element={<VideosPage />}
            />

            <Route
                path={ROUTES.AUDIOS}
                element={<AudiosPage />}
            />
        </Routes>
    )

};

export default RoutesFile;