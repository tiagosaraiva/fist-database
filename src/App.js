import React from 'react';
import RoutesFile from './routes/routes'

function App() {

  return (
    <RoutesFile />
  );
}

export default App;