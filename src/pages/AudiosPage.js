import React, { useEffect, useState ,useRef} from 'react';
import '../css/audios.css'
import HeaderComponent from '../components/HeaderComponent';

function AudiosPage() {

  const [oggFiles, setOggFiles] = useState([]);
  const audioRefs = useRef([]);


  const playRandomAudio = () => {
    const randomIndex = Math.floor(Math.random() * audioRefs.current.length);
    const randomAudio = audioRefs.current[randomIndex];
    if (randomAudio) {
      randomAudio.play();
    }
  };

  const playAllAudios = () => {
    let delay = 0; // Starting delay
    const delayIncrement = 500; // Delay between clips in milliseconds

    audioRefs.current.forEach((audioRef) => {
      if (audioRef) {
        setTimeout(() => {
          audioRef.play();
        }, delay);

        delay += delayIncrement; // Increment delay for next audio
      }
    });
  };

  useEffect(() => {
    fetch('audioFiles.json')
      .then((response) => response.json())
      .then((data) => setOggFiles(data))
      .catch((error) => console.error('An error occurred:', error));
  }, []);

  const handleCopyToClipboard = (path) => {
    navigator.clipboard.writeText(path)
      .then(() => {
        alert('Audio URL copied to clipboard!');
      })
      .catch((err) => {
        alert('Failed to copy URL:', err);
      });
  };

  const stopAllAudios = () => {
    audioRefs.current.forEach((audioRef) => {
      if (audioRef) {
        audioRef.pause();
        audioRef.currentTime = 0;
      }
    });
  };

  return (
    <>     
     <HeaderComponent /> 
    <div style={{ textAlign: 'center', padding: '10px'}}> {/* Center the buttons */}
    <button className="ctn-button" onClick={playAllAudios}>Play All Voices</button>
    <button className="ctn-button" onClick={stopAllAudios}>Stop All Voices</button>
    <button className="ctn-button" onClick={playRandomAudio}>Play Random Voice</button>
    </div>
    <div className="App">
      <div className="video-background">
        <video playsInline="playsinline" autoPlay="autoplay" muted="muted" loop="loop">
          <source src="video.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </div>
      
      <div className="audio-grid">
      
        {/* Left-side cards go here */}
        {oggFiles.slice(0, oggFiles.length / 2).map((file, index) => (
          <div key={index} className="audio-player">
            <p>&ldquo;{file.label}&rdquo;</p> {/* Quotes around the label */}
            <audio controls={true} ref={(el) => (audioRefs.current[index] = el)}>
              <source src={file.path} type="audio/mpeg" />
              Your browser does not support the audio element.
            </audio>
            <button onClick={() => handleCopyToClipboard(window.location.href + file.path)}>Copy link</button>
          </div>
        ))}
      </div>
      <div className="empty-column"></div>
      <div className="audio-grid">
        {/* Right-side cards go here */}
        {oggFiles.slice(oggFiles.length / 2).map((file, index) => (
          <div key={index} className="audio-player">
            <p>&ldquo;{file.label}&rdquo;</p> {/* Quotes around the label */}
            <audio controls={true} ref={(el) => (audioRefs.current[index] = el)}>
              <source src={file.path} type="audio/mpeg" />
              Your browser does not support the audio element.
            </audio>
            <button style={{ color: 'black' }} onClick={() => handleCopyToClipboard(window.location.href + file.path)}>Copy link</button>
          </div>
        ))}
      </div>

    </div></>
  );

}

export default AudiosPage;
