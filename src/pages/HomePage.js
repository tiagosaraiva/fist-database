import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { FaVideo, FaHeadphones } from 'react-icons/fa';
import { ROUTES } from '../routes/routes';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: 'Playfair Display', serif;
  overflow: hidden;
`;

const Option = styled(Link)`
  width: 50%;
  height: 100%;
  color: #ffffff;
  text-decoration: none;
  font-size: 6vw;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.bgColor};
  position: relative;
  transition: 0.3s;

  &:hover {
    opacity: 0.8;
  }
`;

const OrLabel = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: 3vw;
  font-weight: bold;
  color: #ffffff;
  background-color: transparent;
  padding: 10px;
  border-radius: 5px;
`;

const Icon = styled.span`
  font-size: 8vw;
  margin-right: 20px;
`;

const HomePage = () => (
  <Container>
    <Option to={ROUTES.VIDEOS} bgColor="#007bff">
      <Icon><FaVideo /></Icon>
    </Option>
    <Option to={ROUTES.AUDIOS} bgColor="#ffc1076f">
      <Icon><FaHeadphones /></Icon>
    </Option>
    <OrLabel>OR</OrLabel>
  </Container>
);

export default HomePage;
