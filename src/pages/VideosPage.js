import React, { useState,useEffect } from 'react';
import { Link } from 'react-router-dom';
import styled, { keyframes } from 'styled-components';

import { FaRedo, FaHome } from 'react-icons/fa';

const VideoElement = styled.video`
  width: 100%;
  height: 100%;
  object-fit: fill; // Stretch the video to fill its container
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 100%;
  position: fixed; // Added to fix scrolling
  overflow: hidden;
  background-color: #f0f0f0;
  font-family: 'Playfair Display', serif;
  font-size: 24px;
`;

const loadingAnimation = keyframes`
  0% { content: "." }
  25% { content: ".." }
  50% { content: "..." }
  100% { content: "." }
`;

const StartTextContainer = styled.div`
  // Your existing styles
  position: relative;

  &::after {
    content: '.';
    animation: ${loadingAnimation} 1s infinite;
    position: absolute;
    right: 0;
  }
`;

const VideoContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

const IconButton = styled.div`
  position: absolute;
  top: 20px;
  font-size: 36px;
  cursor: pointer;
  z-index: 2; // Same z-index as text
  color: #007bff;

  &:hover {
    opacity: 0.4;
  }
`;

const ReloadButton = styled(IconButton)`
  right: 20px;
`;

const HomeButton = styled(IconButton)`
  left: 20px;
`;

const TopText = styled.div`
  position: absolute;
  top: 20px;
  left: 50%;
  transform: translateX(-50%);
  z-index: 2; // Same z-index as buttons
  font-size: 24px;
  font-weight: bold; // Make the text bold
  font-style: italic; // Make the text italic
  color: #007bff; // Same color as the buttons
`;

const VideoPage = () => {
  const [video, setVideo] = useState(null);
  const [videoFiles, setVideoFiles] = useState([]);
  const [loading, setLoading] = useState(false); // Add loading state

  useEffect(() => {
    fetch('videoFiles.json')
      .then((response) => response.json())
      .then((data) => setVideoFiles(data))
      .catch((error) => console.error('An error occurred:', error));
  }, []);

  const handleRandomVideo = () => {
    setLoading(true); // Set loading state to true

    let newVideo;
    do {
      newVideo = videoFiles[getRandomNumber(videoFiles.length) - 1];
    } while (video && newVideo.path === video.path);

    setVideo(null); // Reset the video state

    // Use setTimeout to allow the component to re-render with the loading state
    setTimeout(() => {
      setVideo(newVideo); // Set the new video
      setLoading(false); // Reset loading state
    }, 0);
  };

  if (loading) {
    return <div>Loading...</div>; // You can replace this with a proper loading indicator
  }

  return (
    <Container>
      {video ? (
        <VideoContainer>
           <TopText>&ldquo;{video.label}&rdquo;</TopText>
          <HomeButton as={Link} to="/">
            <FaHome />
          </HomeButton>
          <ReloadButton onClick={handleRandomVideo}>
            <FaRedo />
          </ReloadButton>
          <VideoElement controls autoPlay>
            <source src={video.path} type="video/mp4" />
            Your browser does not support the video tag.
          </VideoElement>
        </VideoContainer>
      ) : (
        <StartTextContainer>
          {  setTimeout(() => {
      handleRandomVideo();
    }, 1000)}
        </StartTextContainer>
      )}
    </Container>
  );
};

const getRandomNumber = (max) => Math.floor(Math.random() * max) + 1;

export default VideoPage;
